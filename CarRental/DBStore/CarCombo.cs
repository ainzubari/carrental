﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using CarRental.Model;
using CarRental.DBStore;

namespace CarRental.DBStore
{
    public class CarCombo
    {
        public int InsertCarList(InsertCarListModel _ICLM)
        {
            using (SqlConnection conn = new SqlConnection(configInfo.connStr))
            {
                SqlCommand cmd = new SqlCommand("InsertCarList", conn);
                cmd.CommandType = CommandType.StoredProcedure; 
                cmd.Parameters.Add(new SqlParameter("@car_name", _ICLM.car_name));
                cmd.Parameters.Add(new SqlParameter("@description", _ICLM.description));
                cmd.Parameters.Add(new SqlParameter("@car_image", _ICLM.car_image));
                cmd.Parameters.Add(new SqlParameter("@car_model_year", _ICLM.car_model_year));
                cmd.Parameters.Add(new SqlParameter("@car_brand", _ICLM.car_brand));
                cmd.Parameters.Add(new SqlParameter("@color", _ICLM.color));
                cmd.Parameters.Add(new SqlParameter("@car_mileage", _ICLM.car_mileage));
                cmd.Parameters.Add(new SqlParameter("@transmission_type", _ICLM.transmission_type));
                cmd.Parameters.Add(new SqlParameter("@capacity_cc", _ICLM.capacity_cc));
                cmd.Parameters.Add(new SqlParameter("@plate_number", _ICLM.plate_number));
                cmd.Parameters.Add(new SqlParameter("@rate_per_hour", _ICLM.rate_per_hour));
                conn.Open();
                return (cmd.ExecuteNonQuery());
            }
        }
        public int UpdateCarList(InsertCarListModel _ICLM, int car_id)
        {
            using (SqlConnection conn = new SqlConnection(configInfo.connStr))
            {
                SqlCommand cmd = new SqlCommand("UpdateCarList", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@car_id", car_id));
                cmd.Parameters.Add(new SqlParameter("@car_name", _ICLM.car_name));
                cmd.Parameters.Add(new SqlParameter("@description", _ICLM.description));
                cmd.Parameters.Add(new SqlParameter("@car_image", _ICLM.car_image));
                cmd.Parameters.Add(new SqlParameter("@car_model_year", _ICLM.car_model_year));
                cmd.Parameters.Add(new SqlParameter("@car_brand", _ICLM.car_brand));
                cmd.Parameters.Add(new SqlParameter("@color", _ICLM.color));
                cmd.Parameters.Add(new SqlParameter("@car_mileage", _ICLM.car_mileage));
                cmd.Parameters.Add(new SqlParameter("@transmission_type", _ICLM.transmission_type));
                cmd.Parameters.Add(new SqlParameter("@capacity_cc", _ICLM.capacity_cc));
                cmd.Parameters.Add(new SqlParameter("@plate_number", _ICLM.plate_number));
                cmd.Parameters.Add(new SqlParameter("@rate_per_hour", _ICLM.rate_per_hour));
                conn.Open();
                return (cmd.ExecuteNonQuery());
            }
        }

        public int DeleteCarList(int car_id)
        {
           
            using (SqlConnection conn = new SqlConnection(configInfo.connStr))
            {  
                SqlCommand cmd = new SqlCommand("DeleteCarList" , conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@car_id", car_id));
                conn.Open();
                return (cmd.ExecuteNonQuery());
            }
        }

        public DataTable GetCarList()
        {
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

            using (SqlConnection conn = new SqlConnection(configInfo.connStr))
            {
                SqlCommand cmd = new SqlCommand(@"SELECT * FROM car_info", conn);
                cmd.CommandType = CommandType.Text;
                da.SelectCommand = cmd;
                da.Fill(dt);
            }
            return dt;
        }
        public DataTable GetCarRentalCheck(int car_id, DateTime rental_date, TimeSpan rental_on, TimeSpan return_on)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

            using (SqlConnection conn = new SqlConnection(configInfo.connStr))
            {
                SqlCommand cmd = new SqlCommand("GetCarRentalCheck", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@car_id", car_id));
                cmd.Parameters.Add(new SqlParameter("@rental_date", rental_date));
                cmd.Parameters.Add(new SqlParameter("@rental_on", rental_on));
                cmd.Parameters.Add(new SqlParameter("@return_on", return_on));
                da.SelectCommand = cmd;
                da.Fill(dt);
            }
            return dt;
        }
        public int InsertCustomerInfoAndCarRental(InsertCustInfo _ICI, InsertCarRentalModel _ICRM)
        {
            using (SqlConnection conn = new SqlConnection(configInfo.connStr))
            {
                SqlCommand cmd = new SqlCommand("InsertCustomerInfoAndCarRental", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@cust_name", _ICI.customer_name));
                cmd.Parameters.Add(new SqlParameter("@address", _ICI.address));
                cmd.Parameters.Add(new SqlParameter("@contact", _ICI.contact));
                cmd.Parameters.Add(new SqlParameter("@newicnum", _ICI.newicno));
                cmd.Parameters.Add(new SqlParameter("@rental_date", _ICRM.rental_date));
                cmd.Parameters.Add(new SqlParameter("@rental_on", _ICRM.rental_on));
                cmd.Parameters.Add(new SqlParameter("@return_on", _ICRM.return_on));
                cmd.Parameters.Add(new SqlParameter("@car_id", _ICRM.car_id));
                cmd.Parameters.Add(new SqlParameter("@rental_status", _ICRM.rental_status));
                conn.Open();
                return (cmd.ExecuteNonQuery());
            }
        }
        public DataTable GetCarListByID(int car_id)
        {
            SqlDataAdapter da = new SqlDataAdapter();
            DataTable dt = new DataTable();

            using (SqlConnection conn = new SqlConnection(configInfo.connStr))
            {
                SqlCommand cmd = new SqlCommand(@"SELECT * FROM car_info where car_id=" + car_id + "", conn);
                cmd.CommandType = CommandType.Text;
                //cmd.Parameters.Add(new SqlParameter("@car_id", ProductCode));

                da.SelectCommand = cmd;
                da.Fill(dt);
            }
            return dt;
        }

    }


}