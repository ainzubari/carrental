﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace CarRental.DBStore
{
    public class configInfo
    {
        static string sCS = "";

        public static string connStr
        {
            get
            {
                if (sCS == "")
                {
                    sCS = ConfigurationManager.ConnectionStrings["carRental"].ConnectionString;
                }
                return (sCS);
            }
        }
    }
}