﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="InsertCar.aspx.cs" Inherits="CarRental.InsertCar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
   
    
        <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode
            return !(charCode > 31 && (charCode < 48 || charCode > 57));
        }

        function isNumberKey2Decimal(sender, evt) {
            var txt = sender.value;
            var dotcontainer = txt.split('.');
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (!(dotcontainer.length == 1 && charCode == 46) && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>
    <div>
          <!-- 2 column grid layout with text inputs for the first and last names -->
          <div class="row mb-4">
            <div class="col">
              <div class="form-outline">
                  <asp:TextBox ID="txtCarName" runat="server" class="form-control"></asp:TextBox>
                <label class="form-label" for="carName">Nama Kereta</label>
              </div>
            </div>
            <div class="col">
              <div class="form-outline">
                <asp:TextBox ID="txtDescription" runat="server" class="form-control"></asp:TextBox>
                <label class="form-label" for="carDesc">Description</label>
              </div>
            </div>
          </div>
          <div class="row mb-4">
            <div class="col">
              <div class="form-outline">
                  <asp:TextBox ID="txtYear" runat="server" class="form-control" onkeypress="return isNumberKey(event);"></asp:TextBox>
                <label class="form-label" for="carYear">Car Year Model</label>
              </div>
            </div>
            <div class="col">
              <div class="form-outline">
                <asp:TextBox ID="txtBrand" runat="server" class="form-control"></asp:TextBox>
                <label class="form-label" for="carBrand">Car Brand</label>
              </div>
            </div>
          </div>
           <div class="row mb-4">
            <div class="col">
              <div class="form-outline">
                  <asp:TextBox ID="txtColor" runat="server" class="form-control"></asp:TextBox>
                <label class="form-label" for="carColor">Color</label>
              </div>
            </div>
            <div class="col">
              <div class="form-outline">
                <asp:TextBox ID="txtMileage" runat="server" class="form-control" onkeypress="return isNumberKey2Decimal(this, event);"></asp:TextBox>
                <label class="form-label" for="carMileage">Car Mileage</label>
              </div>
            </div>
          </div>
              <div class="row mb-4">
            <div class="col">
              <div class="form-outline">
                  <asp:TextBox ID="txtTransmission" runat="server" class="form-control"></asp:TextBox>
                <label class="form-label" for="carTransmission">Transmisson Type</label>
              </div>
            </div>
            <div class="col">
              <div class="form-outline">
                <asp:TextBox ID="txtCC" runat="server" class="form-control" onkeypress="return isNumberKey2Decimal(this, event);"></asp:TextBox>
                <label class="form-label" for="carCC">Capacity CC</label>
              </div>
            </div>
                 
          </div>
           <div class="row mb-4">
            <div class="col">
              <div class="form-outline">
                  <asp:TextBox ID="txtPlate" runat="server" class="form-control"></asp:TextBox>
                <label class="form-label" for="carPlate">Plate Number</label>
              </div>
            </div>
            <div class="col">
              <div class="form-outline">
                <asp:TextBox ID="txtRate" runat="server" onkeypress="return isNumberKey2Decimal(this, event);" class="form-control"></asp:TextBox>
                <label class="form-label" for="carRate">Rate Per Hour</label>
              </div>
            </div>
                 
          </div>
         <div class="row mb-4">
            <div class="col">
              <div class="form-outline">
                  <asp:FileUpload ID="fileUploadKereta" runat="server" class="form-control" />
                
                <label class="form-label" >Gambar Kereta</label>
                 <asp:Label ID="lblMsg" runat="server" class="form-label"></asp:Label>
              </div>
            </div>
                  
          </div>
          <div >
       
            <p>
                
                <asp:Button ID="btnInsert" class="btn btn-primary btn-block mb-4"  runat="server" Text="Add Car" OnClick="btnInsert_Click" />
            </p>
        </div>


            
     

    </div>
</asp:Content>
