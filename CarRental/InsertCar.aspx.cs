﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CarRental.Model;
using CarRental.DBStore;
using System.Data;

namespace CarRental
{
    public partial class InsertCar : System.Web.UI.Page
    {
        InsertCarListModel _insertCarListModel = new InsertCarListModel();
        CarCombo _carCombo = new CarCombo();
        string type = "";
        string car_id = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Count > 0)
            {
                type = Request.QueryString["type"].ToString();
                if (type == "edit")
                {
                    car_id = Request.QueryString["car_id"].ToString();
                    if (!IsPostBack)
                    {
                        DataTable dtcarlistbyid = _carCombo.GetCarListByID(Convert.ToInt32(car_id));
                        if (dtcarlistbyid.Rows.Count > 0)
                        {
                            txtCarName.Text = dtcarlistbyid.Rows[0]["car_name"].ToString();
                            txtYear.Text = dtcarlistbyid.Rows[0]["car_model_year"].ToString();
                            txtColor.Text = dtcarlistbyid.Rows[0]["color"].ToString();
                            txtDescription.Text = dtcarlistbyid.Rows[0]["description"].ToString();
                            txtPlate.Text = dtcarlistbyid.Rows[0]["plate_number"].ToString();
                            txtBrand.Text = dtcarlistbyid.Rows[0]["car_brand"].ToString();
                            txtCC.Text = dtcarlistbyid.Rows[0]["capacity_cc"].ToString();
                            txtRate.Text = dtcarlistbyid.Rows[0]["rate_per_hour"].ToString();
                            txtMileage.Text = dtcarlistbyid.Rows[0]["car_mileage"].ToString();
                            txtTransmission.Text = dtcarlistbyid.Rows[0]["transmission_type"].ToString();
                        }
                    }
                }
            }

        }

        private void PrepareCarInfo()
        {

            _insertCarListModel.car_name = txtCarName.Text.ToUpper();
            _insertCarListModel.description = txtDescription.Text.ToUpper();
            _insertCarListModel.car_mileage = Convert.ToDecimal(txtMileage.Text);
            _insertCarListModel.transmission_type = txtDescription.Text.ToUpper();
            _insertCarListModel.car_image = fileUploadKereta.FileName;
            _insertCarListModel.car_model_year = Convert.ToInt16(txtYear.Text);
            _insertCarListModel.car_brand = txtBrand.Text.ToUpper();
            _insertCarListModel.color = txtColor.Text.ToUpper();
            _insertCarListModel.capacity_cc = Convert.ToDecimal(txtCC.Text);
            _insertCarListModel.plate_number = txtPlate.Text;
            _insertCarListModel.rate_per_hour = Convert.ToDecimal(txtRate.Text);

        }
        bool ValidateInfoSave()
        {

            string errMsg = "";
            List<string> reqFields = new List<string>();

            try
            {

                #region ButiranMaklumatPakej

                ArrayList ar = new ArrayList();

                ar.Add(new ListItem("txtCarName", " Nama Kereta."));
                ar.Add(new ListItem("fileUploadKereta", " Gambar Kereta."));
                ar.Add(new ListItem("txtDescription", " Description."));
                ar.Add(new ListItem("txtYear", " Tahun Kereta"));
                ar.Add(new ListItem("txtBrand", " Jenama Kereta"));
                ar.Add(new ListItem("txtColor", " Warna Kereta"));
                ar.Add(new ListItem("txtMileage", " Mileage Kereta"));
                ar.Add(new ListItem("txtTransmission", " Jenis Transmisi Kereta"));
                ar.Add(new ListItem("txtCC", " CC Kereta"));
                ar.Add(new ListItem("txtPlate", " Plet Kereta"));
                ar.Add(new ListItem("txtRate", " Kadar Sewaan Sejam"));

                for (int i = 0; i < ar.Count; i++)
                {
                    ListItem li = (ListItem)ar[i];
                    if (li.Text.ToString().Substring(0, 3) == "txt")
                    {

                        if (li.Text == "txtCarName")
                        {
                            if (txtCarName.Text == "")
                            {
                                reqFields.Add(li.Value.ToString());
                            }
                        }
                        if (li.Text == "txtDescription")
                        {
                            if (txtDescription.Text == "")
                            {
                                reqFields.Add(li.Value.ToString());
                            }
                        }
                        if (li.Text == "txtYear")
                        {
                            if (txtYear.Text == "")
                            {
                                reqFields.Add(li.Value.ToString());
                            }
                        }


                        if (li.Text == "txtBrand")
                        {
                            if (txtBrand.Text.Replace(" ", "") == "")
                            {
                                reqFields.Add(li.Value.ToString());
                            }
                        }


                        if (li.Text == "txtColor")
                        {
                            if (txtColor.Text.Replace(" ", "") == "")
                            {
                                reqFields.Add(li.Value.ToString());
                            }
                        }

                        if (li.Text == "txtMileage")
                        {
                            if (txtMileage.Text.Replace(" ", "") == "")
                            {
                                reqFields.Add(li.Value.ToString());
                            }
                        }

                        if (li.Text == "txtTransmission")
                        {
                            if (txtTransmission.Text.Replace(" ", "") == "")
                            {
                                reqFields.Add(li.Value.ToString());
                            }
                        }

                        if (li.Text == "txtCC")
                        {
                            if (txtCC.Text.Replace(" ", "") == "")
                            {
                                reqFields.Add(li.Value.ToString());
                            }
                        }

                        if (li.Text == "txtPlate")
                        {
                            if (txtPlate.Text.Replace(" ", "") == "")
                            {
                                reqFields.Add(li.Value.ToString());
                            }
                        }

                        if (li.Text == "txtRate")
                        {
                            if (txtRate.Text.Replace(" ", "") == "")
                            {
                                reqFields.Add(li.Value.ToString());
                            }
                        }
                    }

                    else
                    {

                        if (li.Text == "fileUploadKereta")
                        {
                            if (fileUploadKereta.FileName == "")
                            {
                                reqFields.Add(li.Value.ToString());
                            }
                        }
                    }



                }
                #endregion

                if (errMsg == "")
                {
                    if (reqFields.Count > 0)
                    {

                        errMsg = "Maklumat Tidak Lengkap:\\n";
                        foreach (string s in reqFields)
                        {
                            errMsg = errMsg + s + "\\n";
                        }
                    }
                }

                if (errMsg != "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Error", "alert('" + errMsg + "');", true);
                    return false;
                }
                else
                    return true;
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Error", "alert('" + ex + "');", true);
                return false;
            }
        }
        private void UploadKereta()
        {
            byte[] fileSize = fileUploadKereta.FileBytes;
            string[] validFileTypes = { "bmp", "png", "jpg", "jpeg", "JPG", "BMP", "PNG", "JPEG" };
            string ext = System.IO.Path.GetExtension(fileUploadKereta.PostedFile.FileName);
            bool isValidFile = false;
            for (int i = 0; i < validFileTypes.Length; i++)
            {
                if (ext == "." + validFileTypes[i])
                {
                    isValidFile = true;
                    lblMsg.Text = "";
                    break;
                }
            }
            if (!isValidFile)
            {
                lblMsg.ForeColor = System.Drawing.Color.Red;
                lblMsg.Text = "* Please Upload File Using Format " +
                               string.Join(",", validFileTypes);
                lblMsg.Focus();
            }
            else if (isValidFile && fileUploadKereta.PostedFile.ContentLength >= 5120000)
            {
                lblMsg.Text = "* File Size Exceeded  5MB";
                lblMsg.Focus();
            }
            else
            {

                string filename = Path.GetFileName(fileUploadKereta.FileName);
                if (String.IsNullOrEmpty(filename)) return;

                try
                {
                    string path = Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["filePath"]);
                    if (System.IO.Directory.Exists(path))
                    {
                        if (System.IO.Directory.Exists(path + "/" + "KERETA"))
                        {
                            path = path + "/" + "KERETA";
                        }
                        else
                        {
                            System.IO.Directory.CreateDirectory(path + "/" + "KERETA");
                            path = path + "/" + "KERETA";
                        }
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(path);
                        path = path + "/" + "KERETA";
                        System.IO.Directory.CreateDirectory(path);
                    }


                    fileUploadKereta.SaveAs(path + "/" + filename);
                    fileUploadKereta.Dispose();


                }

                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Info", "alert('Gambar Gagal Disimpan.'" + ex + "');", true);
                    throw ex;
                }
            }
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            if (ValidateInfoSave())
            {
                PrepareCarInfo();
                UploadKereta();
                if (type == "insert")
                {
                    _carCombo.InsertCarList(_insertCarListModel);
                    ScriptManager.RegisterStartupScript(this, GetType(), "Info", "alert('Car added successfully.'); window.location='Default.aspx'", true);
                }

                if (type == "edit")
                {
                    _carCombo.UpdateCarList(_insertCarListModel, Convert.ToInt16(car_id));
                    ScriptManager.RegisterStartupScript(this, GetType(), "Info", "alert('Car updated successfully.'); window.location='Default.aspx'", true);
                }

            }

        }
    }
}