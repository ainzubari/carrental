﻿
using System.ComponentModel.DataAnnotations;

namespace CarRental.Model
{
    public class InsertCarListModel
    {
		//public string car_id { get; set; }
		[Required(AllowEmptyStrings = false, ErrorMessage = "Car Name is Required")]
		public string car_name { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Car Name is Required")]
		public string description { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Car Name is Required")]
		public string car_image { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Car Name is Required")]
		public decimal car_mileage { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Car Name is Required")]
		public string transmission_type { get; set; }

		[Required( ErrorMessage = "Car Name is Required")]
		public int car_model_year { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Car Name is Required")]
		public string car_brand { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Car Name is Required")]
		public string color { get; set; }

		[Required(ErrorMessage = "Car Name is Required")]
		public decimal capacity_cc { get; set; }
		
		[Required(AllowEmptyStrings = false, ErrorMessage = "Car Name is Required")]
		public string plate_number { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Car Name is Required")]
		public decimal rate_per_hour { get; set; }
	}
}