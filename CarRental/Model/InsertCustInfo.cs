﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarRental.Model
{
    public class InsertCustInfo
    {
        public string customer_name { get; set; }
        public string address { get; set; }
        public string contact { get; set; }
        public string newicno { get; set; }
    }
}