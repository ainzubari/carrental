﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CarRental.Model
{
    public class InsertCarRentalModel
    {
		public DateTime rental_date { get; set; }
		public TimeSpan return_on { get; set; }
		public TimeSpan rental_on { get; set; }
		public int car_id { get; set; }
		public int customer_id { get; set; }
		public Boolean rental_status { get; set; }
	}
}