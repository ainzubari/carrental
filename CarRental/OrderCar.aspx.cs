﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CarRental.DBStore;
using CarRental.Model;

namespace CarRental
{
    public partial class OrderCar : System.Web.UI.Page
    {

        InsertCarRentalModel _InsertCarRentalModel = new InsertCarRentalModel();
        InsertCustInfo _InsertCustInfo = new InsertCustInfo();
        CarCombo _CarCombo = new CarCombo();
        string car_id = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString.Count > 0)
            {

                car_id = Request.QueryString["car_id"].ToString();
            }

            if (!Page.IsPostBack)
            {
                calFromDate.SelectedDate = DateTime.Now;
                txtFromDate.Text = calFromDate.SelectedDate.ToString("dd/MM/yyyy");

                DataTable dtcarlistbyid = _CarCombo.GetCarListByID(Convert.ToInt32(car_id));
                if (dtcarlistbyid.Rows.Count > 0)
                {
                    lblCarName.Text = dtcarlistbyid.Rows[0]["car_name"].ToString();
                    lblCarYear.Text = dtcarlistbyid.Rows[0]["car_model_year"].ToString();
                    lblColour.Text = dtcarlistbyid.Rows[0]["color"].ToString();
                    lblDescription.Text = dtcarlistbyid.Rows[0]["description"].ToString();
                    lblPlateNo.Text = dtcarlistbyid.Rows[0]["plate_number"].ToString();
                    lblBrand.Text = dtcarlistbyid.Rows[0]["car_brand"].ToString();
                    lblCarCC.Text = dtcarlistbyid.Rows[0]["capacity_cc"].ToString();
                    lblRatePerHour.Text = "RM" + dtcarlistbyid.Rows[0]["rate_per_hour"].ToString(); ;
                }
            }

        }
        protected void lnkbtnFromDate_Click(object sender, EventArgs e)
        {
            calFromDate.Visible = true;
        }

        protected void btnCheck_Click(object sender, EventArgs e)
        {
            var TimeOn = TimeSpan.Parse(ddlTimeOn.SelectedValue);
            var TimeReturn = TimeSpan.Parse(ddlTimeBack.SelectedValue);
            DateTime oDate = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", null);

            if (TimeReturn <= TimeOn)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Info", "alert('Sila Masukkan Waktu Yang Betul');", true);
            }
            else
            {
                DataTable dtcarlist = _CarCombo.GetCarRentalCheck(Convert.ToInt32(car_id), oDate, TimeOn, TimeReturn);
                if (dtcarlist.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Info", "alert('Tempahan Sudah Penuh Pada Tarikh dan Masa Tersebut');", true);
                }
                else
                {
                    btnCheck.Visible = false;
                    btnEdit.Visible = true;
                    pnlView.Visible = true;
                    txtName.Focus();
                    ScriptManager.RegisterStartupScript(this, GetType(), "Info", "alert('Tempahan Anda Tersedia Pada Tarikh Tersebut. Sila Lengkapkan Maklumat Anda Untuk Ke Proses Seterusnya');", true);
                }
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            btnCheck.Visible = true;
            btnEdit.Visible = false;
            pnlView.Visible = false;
            txtName.Text = "";
            txtAddress.Text = "";
            txtNewIC.Text = "";
            txtHPNo.Text = "";
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            if (ValidateInfoSave())
            {
                PrepareCarOrder();
                PrepareCustomerInfo();

                var TimeOn = TimeSpan.Parse(ddlTimeOn.SelectedValue);
                var TimeReturn = TimeSpan.Parse(ddlTimeBack.SelectedValue);
                DateTime oDate = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", null);
                if (TimeReturn <= TimeOn)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Info", "alert('Sila Masukkan Waktu Yang Betul');", true);
                }
                else
                {
                    DataTable dtcarlist = _CarCombo.GetCarRentalCheck(Convert.ToInt32(car_id), oDate, TimeOn, TimeReturn);
                    if (dtcarlist.Rows.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Info", "alert('Tempahan Sudah Penuh Pada Tarikh dan Masa Tersebut');", true);
                    }
                    else
                    {

                        _CarCombo.InsertCustomerInfoAndCarRental(_InsertCustInfo, _InsertCarRentalModel);
                        ScriptManager.RegisterStartupScript(this, GetType(), "Info", "alert('Info Kereta Telah Berjaya Ditambah');window.location = 'Default.aspx';", true);
                    }

                }
            }
        }

        private void PrepareCarOrder()
        {
            _InsertCarRentalModel.car_id = Convert.ToInt32(car_id);
            _InsertCarRentalModel.rental_date = DateTime.ParseExact(txtFromDate.Text, "dd/MM/yyyy", null);
            _InsertCarRentalModel.rental_on = TimeSpan.Parse(ddlTimeOn.SelectedValue);
            _InsertCarRentalModel.return_on = TimeSpan.Parse(ddlTimeBack.SelectedValue);
            _InsertCarRentalModel.rental_status = true;
        }

        private void PrepareCustomerInfo()
        {
            _InsertCustInfo.customer_name = txtName.Text.ToUpper();
            _InsertCustInfo.contact = txtHPNo.Text;
            _InsertCustInfo.address = txtAddress.Text;
            _InsertCustInfo.newicno = txtNewIC.Text;

        }
        bool ValidateInfoSave()
        {

            string errMsg = "";
            List<string> reqFields = new List<string>();

            try
            {

                #region ButiranMaklumatPakej
                ArrayList ar = new ArrayList();
               

                ar.Add(new ListItem("txtName", " Nama."));
                ar.Add(new ListItem("txtHPNo", " No HP"));
                ar.Add(new ListItem("txtAddress", " Alamat."));
                ar.Add(new ListItem("txtNewIC", " My Kad/Passport"));
                ar.Add(new ListItem("ddlTimeOn", " Masa Tempahan"));
                ar.Add(new ListItem("ddlTimeBack", " Masa Pulang"));

                for (int i = 0; i < ar.Count; i++)
                {
                    ListItem li = (ListItem)ar[i];
                    if (li.Text.ToString().Substring(0, 3) == "txt")
                    {

                        if (li.Text == "txtName")
                        {
                            if (txtName.Text == "")
                            {
                                reqFields.Add(li.Value.ToString());
                            }
                        }

                        if (li.Text == "txtNewIC")
                        {
                            if (txtNewIC.Text == "")
                            {
                                reqFields.Add(li.Value.ToString());
                            }
                        }
                        if (li.Text == "txtAddress")
                        {
                            if (txtAddress.Text == "")
                            {
                                reqFields.Add(li.Value.ToString());
                            }
                        }

                    }

                    if (li.Text.ToString().Substring(0, 3) == "ddl")
                    {
                        if (li.Text == "ddlTimeOn")
                        {
                            if (ddlTimeOn.SelectedValue == "")
                            {
                                reqFields.Add(li.Value.ToString());
                            }
                        }

                        if (li.Text == "ddlTimeBack")
                        {
                            if (ddlTimeBack.SelectedValue == "")
                            {
                                reqFields.Add(li.Value.ToString());
                            }
                        }

                    }



                }
                #endregion

                if (errMsg == "")
                {
                    if (reqFields.Count > 0)
                    {

                        errMsg = "Maklumat Tidak Lengkap:\\n";
                        foreach (string s in reqFields)
                        {
                            errMsg = errMsg + s + "\\n";
                        }
                    }
                }

                if (errMsg != "")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Error", "alert('" + errMsg + "');", true);
                    return false;
                }
                else
                    return true;
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Error", "alert('" + ex + "');", true);
                return false;
            }
        }

        protected void calFromDate_SelectionChanged(object sender, EventArgs e)
        {
            txtFromDate.Text = calFromDate.SelectedDate.ToString("dd/MM/yyyy");
            txtFromDate.Focus();
            calFromDate.Visible = false;
        }
    }
}