﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OrderCar.aspx.cs" Inherits="CarRental.OrderCar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
              <script type="text/javascript">
              function isNumberKey(evt) {
                  var charCode = (evt.which) ? evt.which : evt.keyCode
                  return !(charCode > 31 && (charCode < 48 || charCode > 57));
              }

              function isNumberKey2Decimal(sender, evt) {
                  var txt = sender.value;
                  var dotcontainer = txt.split('.');
                  var charCode = (evt.which) ? evt.which : event.keyCode;
                  if (!(dotcontainer.length == 1 && charCode == 46) && charCode > 31 && (charCode < 48 || charCode > 57))
                      return false;

                  return true;
              }
          </script>
   <div class="border-top my-3"></div>
        <h3>Maklumat Kenderaan</h3>
        <div class="border-top my-3"></div>
    <div>
          <!-- 2 column grid layout with text inputs for the first and last names -->
          <div class="row mb-4">
            <div class="col">
              <div class="form-outline">
                  <asp:Label ID="lblCarName" runat="server" class="form-control" ></asp:Label>
                <label class="form-label" for="carName">Nama Kereta</label>
              </div>
            </div>
            <div class="col">
              <div class="form-outline">
                <asp:Label ID="lblDescription" runat="server" class="form-control" ></asp:Label>
                <label class="form-label" for="carDesc">Description</label>
              </div>
            </div>
          </div>

          <div class="row mb-4">
            <div class="col">
              <div class="form-outline">
                 
                  <asp:Label ID="lblCarYear" runat="server" class="form-control" ></asp:Label>
                <label class="form-label" for="carYear">Tahun Kereta</label>
              </div>
            </div>

              

 
            <div class="col">
              <div class="form-outline">
                   <asp:Label ID="lblColour" runat="server" class="form-control" ></asp:Label>
                
                <label class="form-label" for="carColor">Warna Kereta</label>
              </div>
            </div>
          </div>

          <div class="row mb-4">
            <div class="col">
              <div class="form-outline">
                  <asp:Label ID="lblPlateNo" runat="server" class="form-control" ></asp:Label>
                <label class="form-label" for="carPlateNo">Plet Kereta</label>
              </div>
            </div>
            <div class="col">
              <div class="form-outline">
                <asp:Label ID="lblRatePerHour" runat="server" class="form-control" ></asp:Label>
                <label class="form-label" for="carRatePerHr">Kadar Sewaan Sejam</label>
              </div>
            </div>
          </div>

          <div class="row mb-4">
            <div class="col">
              <div class="form-outline">
                  <asp:Label ID="lblCarCC" runat="server" class="form-control" ></asp:Label>
                  
                <label class="form-label" for="carCC">CC Kereta</label>
              </div>
            </div>
            <div class="col">
              <div class="form-outline">
                 <asp:Label ID="lblBrand" runat="server" class="form-control"></asp:Label>
                <label class="form-label" for="carBrand">Jenama</label>
              </div>
            </div>
          </div>
        <div class="border-top my-3"></div>
        <h3>Sila Pilih Tarikh Dan Masa</h3>
        <div class="border-top my-3"></div>
        <div class="row mb-4">
  <div class="col">
              <div class="form-outline">
                  <asp:TextBox runat="server" class="form-control" ID="txtFromDate" placeholder="Pilih Tarikh" ReadOnly="true"></asp:TextBox>                                         
                  <asp:Calendar ID="calFromDate" SelectedDate="<%# DateTime.Today %>" runat="server" Visible="False" OnSelectionChanged="calFromDate_SelectionChanged"></asp:Calendar>                                                             
                <label class="form-label" for="txtFromDate"> 
                    
                    <asp:LinkButton runat="server" ID="lnkbtnFromDate" OnClick="lnkbtnFromDate_Click" class="" Text="Tarikh Tempahan"></asp:LinkButton></label>
             
                  
              </div>
        </div>
               <div class="col">
              <div class="form-outline">
               <asp:DropDownList runat="server" ID="ddlTimeOn" class="form-control">
                   <asp:ListItem Value="" >-Masa Tempahan-</asp:ListItem>
                   <asp:ListItem>09:00</asp:ListItem>
                   <asp:ListItem>10:00</asp:ListItem>
                   <asp:ListItem>11:00</asp:ListItem>
                   <asp:ListItem>12:00</asp:ListItem>
                   <asp:ListItem>13:00</asp:ListItem>
                   <asp:ListItem>14:00</asp:ListItem>
                   <asp:ListItem>15:00</asp:ListItem>
                   <asp:ListItem>16:00</asp:ListItem>
                   <asp:ListItem>17:00</asp:ListItem>
                   <asp:ListItem>18:00</asp:ListItem>
                   <asp:ListItem>19:00</asp:ListItem>
                   <asp:ListItem>20:00</asp:ListItem>
                   <asp:ListItem>21:00</asp:ListItem>
                  </asp:DropDownList> <asp:DropDownList runat="server" ID="ddlTimeBack" class="form-control">
                   <asp:ListItem Value="">-Masa Pulang-</asp:ListItem>
                   <asp:ListItem>09:00</asp:ListItem>
                   <asp:ListItem>10:00</asp:ListItem>
                   <asp:ListItem>11:00</asp:ListItem>
                   <asp:ListItem>12:00</asp:ListItem>
                   <asp:ListItem>13:00</asp:ListItem>
                   <asp:ListItem>14:00</asp:ListItem>
                   <asp:ListItem>15:00</asp:ListItem>
                   <asp:ListItem>16:00</asp:ListItem>
                   <asp:ListItem>17:00</asp:ListItem>
                   <asp:ListItem>18:00</asp:ListItem>
                   <asp:ListItem>19:00</asp:ListItem>
                   <asp:ListItem>20:00</asp:ListItem>
                   <asp:ListItem>21:00</asp:ListItem>
                  </asp:DropDownList>
               
              </div>
            </div>
            </div>
         
         
          

                

          <!-- Checkbox -->
         

          <!-- Submit button -->
    
            
  
        </div>

         <div class="row mb-4">
              <div class="col">
              <div class="form-outline">
                 <asp:Button runat="server" ID="btnCheck" Text="Semak" class="btn btn-primary btn-block mb-4" OnClick="btnCheck_Click" />
                  <asp:Button runat="server" ID="btnEdit" Text="Edit" Visible="false" class="btn btn-primary btn-block mb-4"  OnClick="btnEdit_Click" />
              </div>
            </div>
            
          </div>
        <asp:Panel ID="pnlView" runat="server" Visible="false">
         <div class="border-top my-3"></div>
        <h3>Maklumat Tempahan</h3>
        <div class="border-top my-3"></div>
          

         <div class="row mb-12">
            <div class="col">
              <div class="form-outline">
                 <asp:TextBox runat="server" class="form-control" ID="txtName"  ></asp:TextBox>
                <label class="form-label" for="carName">Nama</label>
              </div>
            </div>
          
          </div>
          <div class="row mb-4">
            <div class="col">
              <div class="form-outline">           
                  <asp:TextBox runat="server" class="form-control" ID="txtNewIC"  ></asp:TextBox>
                <label class="form-label" for="carYear">My Kad/Passport</label>
              </div>
            </div>

              

 
            <div class="col">
              <div class="form-outline">
                   <asp:TextBox runat="server" class="form-control" ID="txtHPNo"  ></asp:TextBox>
                <label class="form-label" for="carColor">No HP</label>
              </div>
            </div>
          </div>
          <div class="row mb-12">
            <div class="col">
              <div class="form-outline">     
                    <asp:TextBox runat="server" class="form-control" ID="txtAddress"  ></asp:TextBox>
                <label class="form-label" for="carYear">Alamat</label>
              </div>
            </div>

              

 
            
          </div>
              <asp:Button runat="server" ID="btnRegister" Text="Tempah" class="btn btn-primary btn-block mb-4" OnClick="btnRegister_Click" />
        </asp:Panel>
  
   </div>
</asp:Content>
