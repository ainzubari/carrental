Upload Database

1) Right Click "Databases"
2) Click "Restore  Database..."
3) On Source
   - Click on "Device:"
   - Click triple dot (...)
4) Pop Name (Select backup devices)
   - Click "Add" Button
   - Navigate to the downloaded database
5) Change the extenstion file (On bottom popup)
   - Change to "AllFile(*)"
   - Click the relavent database
6) Click "OK"
7) Destination Database
  - Change the database name (if you have same name, if not..just proceed)
8) Then "OK".....


--Side Note--
Dont forget to change database name in web.config

